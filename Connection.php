<?php

/*
 * The Connection file is used to get the connection to the database,
 * this file uses the singleton pattern so that no more than one instance of a
 * connection can be created at one time. If a connection has already been
 * created then it returns that connection, otherwise it creates a new
 * connection and returns that.
 */

class Connection {

//                $host = "daneel";
//                $database = "N00146098";
//                $username = "N00146098";
//                $password = "N00146098";

    private static $connection = NULL;

    public static function getInstance() {

        // if the connection is NULL create a new connection, otherwise return the current instance
        if (Connection::$connection === NULL) {
            // connection information from the server, DB
            $host = "localhost";
            $database = "eventscompany";
            $username = "dbuser";
            $password = "dbpassword";

            $dsn = "mysql:host=" . $host . ";dbname=" . $database;
            Connection::$connection = new PDO($dsn, $username, $password);
            if (!Connection::$connection) {
                die("Could not connect to database");
            }
        }

        return Connection::$connection;
    }

}
