<?php

require_once 'Constants.php';

/*
 * Function to get the current time in the date format its stored as in the DB
 */

function getDatetimeNow() {
    $tz_object = new DateTimeZone('Europe/London');
    $datetime = new DateTime();
    // set the new DateTime objects timezone to UK/Ireland
    $datetime->setTimezone($tz_object);
    // return the date formatted in the same way as stored in MySQL
    return $datetime->format('Y\-m\-d\ h:i:s');
}

// used for setting the value within the form
function echoOutput($array, $fieldName) {
    if (isset($array) && isset($array[$fieldName])) {
        echo $array[$fieldName];
    }
}

// used for setting a checked radio buttons state to checked
function echoChecked($array, $fieldName, $value) {
    if (isset($array[$fieldName]) && $array[$fieldName] == $value) {
        echo 'checked="checked"';
    }
}

// used for setting a select elements value
function echoSelected($array, $fieldName, $value) {
    if (isset($array[$fieldName]) && $array[$fieldName] == $value) {
        echo 'selected="selected"';
    }
}

function validate(&$formdata, &$errors) {

    /*
     *  filter all input from post and store each in a formadata array (sanitized)
     *  with keys which are the columns names from constants.php
     */
    $formdata[COLUMN_LOC_ID] = filter_input(INPUT_POST, COLUMN_LOC_ID, FILTER_SANITIZE_NUMBER_INT);
    $formdata[COLUMN_LOC_NAME] = filter_input(INPUT_POST, COLUMN_LOC_NAME, FILTER_SANITIZE_STRING);
    $formdata[COLUMN_LOC_ADDRESS] = filter_input(INPUT_POST, COLUMN_LOC_ADDRESS, FILTER_SANITIZE_STRING);
    $formdata[COLUMN_LOC_CAPACITY] = filter_input(INPUT_POST, COLUMN_LOC_CAPACITY, FILTER_SANITIZE_NUMBER_INT);
    $formdata[COLUMN_LOC_MAN_NAME] = filter_input(INPUT_POST, COLUMN_LOC_MAN_NAME, FILTER_SANITIZE_STRING);
    $formdata[COLUMN_LOC_MAN_EMAIL] = filter_input(INPUT_POST, COLUMN_LOC_MAN_EMAIL, FILTER_SANITIZE_EMAIL);
    $formdata[COLUMN_LOC_MAN_PHONE] = filter_input(INPUT_POST, COLUMN_LOC_MAN_PHONE, FILTER_SANITIZE_STRING);
    $formdata[COLUMN_LOC_SEATING] = filter_input(INPUT_POST, COLUMN_LOC_SEATING, FILTER_SANITIZE_NUMBER_INT);
    $formdata[COLUMN_LOC_TYPE] = filter_input(INPUT_POST, COLUMN_LOC_TYPE, FILTER_SANITIZE_STRING);


    /*
     * The location name, location address, location manager name, and location manager phone number
     * fields should not be empty, if so display the error message that they are required fields.
     */

    $name = $formdata[COLUMN_LOC_NAME];
    if ($name === NULL || $name === FALSE || $name === "") {
        $errors[COLUMN_LOC_NAME] = "Location Name is required";
    }

    $address = $formdata[COLUMN_LOC_ADDRESS];
    if ($address === NULL || $address === FALSE || $address === "") {
        $errors[COLUMN_LOC_ADDRESS] = "Location Address is required";
    }

    $lmname = $formdata[COLUMN_LOC_MAN_NAME];
    if ($lmname === NULL || $lmname === FALSE || $lmname === "") {
        $errors[COLUMN_LOC_MAN_NAME] = "Location Manager Name is required";
    }

    $lmphonenumber = $formdata[COLUMN_LOC_MAN_PHONE];
    if ($lmphonenumber === NULL || $lmphonenumber === FALSE || $lmphonenumber === "") {
        $errors[COLUMN_LOC_MAN_PHONE] = "Location Manager Phone Number is required";
    } else if (!is_numeric($lmphonenumber)) { // if the phone number is not a number (phone number) display error message
        $errors[COLUMN_LOC_MAN_PHONE] = "Location Manager Phone Number must be a valid number (numbers only)";
    }

    /*
     * Again check that the value for location managers email is not empty,
     * if it is then display an error message that it is required,
     * if its not empty then check that it is of valid email format, if it's not
     * valid format display so in the error message.
     */
    $lmemail = $formdata[COLUMN_LOC_MAN_EMAIL];
    if ($lmemail !== NULL && $lmemail !== FALSE && $lmemail !== "") {
        // check the format of the email to make sure it is of valid format
        if (!filter_var($lmemail, FILTER_VALIDATE_EMAIL)) {
            $errors[COLUMN_LOC_MAN_EMAIL] = "Location Manager Email must be of valid format";
        }
    } else {
        $errors[COLUMN_LOC_MAN_EMAIL] = "Location Manager Email is required"; // otherwise the email is empty
    }

    $capacity = $formdata[COLUMN_LOC_CAPACITY];
    // check that capacity is not empty
    if ($capacity !== NULL && $capacity !== FALSE && $capacity !== "") {
        $capacityInt = intval($capacity);
        // if its not empty make sure its a valid capacity (not 0 or less)
        if ($capacityInt <= 0) {
            $errors[COLUMN_LOC_CAPACITY] = "Location Capacity of 0 or less is invalid";
        }
    } else {
        $errors[COLUMN_LOC_CAPACITY] = "Location Capacity is required"; // otherwise the age is empty
    }

    /**
     * Whether seating is available or not, as this is a radio button input type
     * first check that at least one option is cehcked,  then create an array with the
     * valid options then check if either value is the one from the value in formdata array.
     * If not then no option was selected.
     */
    $lSeating = $formdata[COLUMN_LOC_SEATING];
    if ($lSeating !== NULL && $lSeating !== FALSE && $lSeating !== "") {
        $validSeating = array('1', '0');
        if (!in_array($formdata[COLUMN_LOC_SEATING], $validSeating)) {
            $errors[COLUMN_LOC_SEATING] = 'A seating option must be selected!';
        }
    } else {
        $errors[COLUMN_LOC_SEATING] = 'A valid seating option must be selected!';
    }

    /*
     * Same as the above however
     */
    $lType = $formdata[COLUMN_LOC_TYPE];
    if ($lType !== NULL && $lType !== FALSE && $lType !== "") {
        $validTypes = array('Indoor', 'Outdoor', 'Both');
        if (!in_array($formdata[COLUMN_LOC_TYPE], $validTypes)) {
            $errors[COLUMN_LOC_TYPE] = 'A valid Location Type must be selected!';
        }
    } else {
        $errors[COLUMN_LOC_TYPE] = 'No Location Type was selected';
    }

    /*
     * Create the time it was added/updated
     */
    $formdata[COLUMN_LOC_LAST_UPDATED] = getDatetimeNow();
}
