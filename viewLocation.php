<?php
require_once 'Constants.php';
require_once 'Connection.php';
require_once 'LocationTableGateway.php';

// make sure that the id is set in the GET array
if (!isset($_GET[COLUMN_LOC_ID])) {
    die("Illegal request");
}
// to keep it secure filter the id
$id = filter_input(INPUT_GET, COLUMN_LOC_ID, FILTER_SANITIZE_NUMBER_INT);

$connection = Connection::getInstance();
$gateway = new LocationTableGateway($connection);

// get the row data by the locations ID
$statement = $gateway->getLocationById($id);

// get the rows data
$row = $statement->fetch(PDO::FETCH_ASSOC);
if (!$row) {
    die("Illegal request");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/tableStyles.css"/>
        <script type="text/javascript" src="js/deleteLocationRow.js"></script>
        <title>View Location</title>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Capacity</th>
                    <th>Location Manager Name</th>
                    <th>Location Manager Email</th>
                    <th>Location Manager Phone Number</th>
                    <th>Location Type</th>
                    <th>Location Seating</th>
                    <th>Last Updated</th>
                    <th></th><!-- actions column -->
                </tr>
            </thead>
            <tbody>
                <?php
                echo '<tr>';
                echo '<td>' . $row[COLUMN_LOC_ID] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_NAME] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_ADDRESS] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_CAPACITY] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_MAN_NAME] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_MAN_EMAIL] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_MAN_PHONE] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_TYPE] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_SEATING] . '</td>';
                echo '<td>' . $row[COLUMN_LOC_LAST_UPDATED] . '</td>';
                // cell which contains the edit and delete action
                echo '<td>';
                // use the id name as in the table rather than the parameter for get being id
                echo '<a href="formEditLocation.php?' . COLUMN_LOC_ID . '=' . $row[COLUMN_LOC_ID] . '" class="action_button"><img class="img_btn" src="./images/edit.png"></a>';
                echo '<a href="deleteLocation.php?' . COLUMN_LOC_ID . '=' . $row[COLUMN_LOC_ID] . '" class="action_button delete_button"><img class="img_btn" src="./images/delete.png"></a>';
                echo '</td>';
                echo '</tr>';
                ?>
            </tbody>
        </table>
    </body>
</html>
