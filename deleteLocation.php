<?php

/*
 * Deletes the location from the database.
 */

require_once 'Constants.php';
require_once 'Connection.php';
require_once 'LocationTableGateway.php';

if (!isset($_GET) || !isset($_GET[COLUMN_LOC_ID])) {
    die('Invalid request');
}

// to keep it secure filter the id
$id = filter_input(INPUT_GET, COLUMN_LOC_ID, FILTER_SANITIZE_NUMBER_INT);

$connection = Connection::getInstance();
$gateway = new LocationTableGateway($connection);

$gateway->deleteLocation($id);

header("Location: index.php");
?>