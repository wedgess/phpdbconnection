window.onload = function () { // when the window loads
    // gte all elements with the class name delete_button and store them in an array
    var deleteBtns = document.getElementsByClassName('delete_button');

    // loop through each button
    for (var i = 0; i < deleteBtns.length; i++) {

        /*
         *  add an event listener for each button, when a button is clicked show
         *  a confirm dialog asking the user if they want to delete the item
         *  if they don't then cancel the deafult link which is to delete the 
         *  location.(deleteLocation.php)
         */
        deleteBtns[i].addEventListener("click", function (event) {
            if (!confirm("Do you really want to delete this item?")) {
                event.preventDefault();
            }
        });
    }
};
