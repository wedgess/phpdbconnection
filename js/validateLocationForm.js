window.onload = function () {
    /*
     * Javascript validation of the formCreateLocation.php and formEditLocation.php
     */

    // function checks whether or not the value is a number
    function isNumber(num) {
        return !isNaN(parseInt(num)) && isFinite(num);
    }

    // function checks whether an email is enetered in a valid format
    function isValidEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }


    var submitBtn = document.getElementById("submitBtn");

    submitBtn.addEventListener('click', function (event) {

        var isValid = true;

        // get a reference to each of the error span elelments
        var nameErrorElement = document.getElementById('lNameError');
        var addressErrorElement = document.getElementById('lAddressError');
        var capacityErrorElement = document.getElementById('lCapacityError');
        var locManagerNameErrorElement = document.getElementById('lmNameError');
        var locManagerEmailErrorElement = document.getElementById('lmEmailError');
        var locManagerPhoneNumberErrorElement = document.getElementById('lmPhoneNumberError');
        var locSeatingErrorElement = document.getElementById('lSeatingError');
        var locTypeErrorElement = document.getElementById('lTypeError');

        // clear previous errors
        nameErrorElement.innerHTML = "";
        addressErrorElement.innerHTML = "";
        capacityErrorElement.innerHTML = "";
        locManagerNameErrorElement.innerHTML = "";
        locManagerEmailErrorElement.innerHTML = "";
        locManagerPhoneNumberErrorElement.innerHTML = "";
        locSeatingErrorElement.innerHTML = "";
        locTypeErrorElement.innerHTML = "";

        // reference to the input elements
        var nameInput = document.getElementById('lNameInput');
        var addressInput = document.getElementById('lAddressInput');
        var capacityInput = document.getElementById('lCapacityInput');
        var locManagerNameInput = document.getElementById('lmNameInput');
        var locManagerEmailInput = document.getElementById('lmEmailInput');
        var locManagerPhoneNumberInput = document.getElementById('lmPhoneNumberInput');

        var locSeatingRadioButtons = document.getElementsByName('lSeatingAvailable');
        var locTypeSelectOption = document.getElementById('lTypeInput');

        // refernce to input fields values
        var name = nameInput.value;
        var address = addressInput.value;
        var capacity = capacityInput.value;
        var locManagerName = locManagerNameInput.value;
        var locManagerEmail = locManagerEmailInput.value;
        var locManagerPhoneNumber = locManagerPhoneNumberInput.value;

        // name is required
        if (name === "") {
            nameErrorElement.innerHTML = "Location Name is required";
            isValid = false;
        }

        // address is required
        if (address === "") {
            addressErrorElement.innerHTML = "Location Address is required";
            isValid = false;
        }


        // capacity is required
        if (capacity === "") {
            capacityErrorElement.innerHTML = "Location Capacity is required";
            isValid = false;
        } else {
            if (!isNumber(capacity)) {
                capacityErrorElement.innerHTML = "Location Capacity must be an integer";
                isValid = false;
            } else {
                var capacityNum = parseInt(capacity);
                if (capacityNum <= 0) {
                    capacityErrorElement.innerHTML = "Location Capacity of 0 or less is invalid";
                    isValid = false;
                }
            }
        }

        // loc manager name is required
        if (locManagerName === "") {
            locManagerNameErrorElement.innerHTML = "Location Manager Name is required";
            isValid = false;
        }

        // loc manager phone number is required
        if (locManagerPhoneNumber === "") {
            locManagerPhoneNumberErrorElement.innerHTML = "Location Manager Phone Number is required";
            isValid = false;
        } else if (!isNumber(locManagerPhoneNumber)) { // if the phone number is not a valid number then add error message
            locManagerPhoneNumberErrorElement.innerHTML = "Location Manager Phone Number must be a valid number (numbers only)";
        }

        if (locManagerEmail === "") {
            locManagerEmailErrorElement.innerHTML = "Location Manager Email is required";
            isValid = false;
        } else {
            if (!isValidEmail(locManagerEmail)) {
                locManagerEmailErrorElement.innerHTML = "Location Manager Email must be of valid format";
                isValid = false;
            }
        }

        var seatingSelected = false;
        for (var i = 0; i < locSeatingRadioButtons.length; i++) { // loop thorugh each radio button
            if (locSeatingRadioButtons[i].checked) { // if the radio button in this position is selected
                seatingSelected = true;
                break;
            }
        }
        if (!seatingSelected) {
            locSeatingErrorElement.innerHTML = 'A Seating option must be selected!';
            isValid = false;
        }

        if (locTypeSelectOption.selectedIndex === 0) {
            locTypeErrorElement.innerHTML = 'A valid Location Type must be selected!';
            isValid = false;
        }

//        console.log("IS valid " + isValid);

        if (!isValid) {
            event.preventDefault();
        }
    });
};


