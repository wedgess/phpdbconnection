<?php

/*
 * This file is called on the submit button click of the formEditLocation.php
 * It calls the validate function of the validateLocation.php file to sanitize and
 * validate the forms data.
 */

//print_r($_POST);

require_once 'Connection.php';
require_once 'LocationTableGateway.php';
require_once 'Constants.php';
require_once 'validateLocation.php';

$formdata = array();
$errors = array();

validate($formdata, $errors);

if (empty($errors)) {

    // filter the location id received from $_POST as int
    $id = filter_input(INPUT_POST, COLUMN_LOC_ID, FILTER_SANITIZE_NUMBER_INT);

    $name = $formdata[COLUMN_LOC_NAME];
    $address = $formdata[COLUMN_LOC_ADDRESS];
    $capacity = $formdata[COLUMN_LOC_CAPACITY];
    $lmName = $formdata[COLUMN_LOC_MAN_NAME];
    $lmEmail = $formdata[COLUMN_LOC_MAN_EMAIL];
    $lmPhoneNumber = $formdata[COLUMN_LOC_MAN_PHONE];
    $lType = $formdata[COLUMN_LOC_TYPE];
    $lSeating = $formdata[COLUMN_LOC_SEATING];
    $lUpdated = $formdata[COLUMN_LOC_LAST_UPDATED];

    // create the location object as its easier to pass to the function
    $location = new Location($id, $name, $address, $capacity, $lmName, $lmEmail, $lmPhoneNumber, $lType, $lSeating, $lUpdated);

    $connection = Connection::getInstance();
    $gateway = new LocationTableGateway($connection);
    $gateway->updateLocation($location); // update the location calling LocationtableGateways updateLocation function
    header('Location: index.php'); // redirect the user back to index.php page
} else {
    require 'formEditLocation.php'; // if errors are present go back to the formEditLocation.php page
}



