<?php

/*
 * Constants.php stores the tables name, and all of its column names
 * I use a constants file so that mistakes will not be made with columns
 * and also if the names where to change I can change them in one place
 * rather than having to change them in multiple files and multiple places.
 */

// Location Table name and column names
define("LOCATION_TABLE_NAME", "web_location");
define("COLUMN_LOC_ID", "lID");
define("COLUMN_LOC_NAME", "lName");
define("COLUMN_LOC_ADDRESS", "lAddress");
define("COLUMN_LOC_CAPACITY", "lCapacity");
define("COLUMN_LOC_MAN_NAME", "lmName");
define("COLUMN_LOC_MAN_EMAIL", "lmEmail");
define("COLUMN_LOC_MAN_PHONE", "lmPhoneNumber");
define("COLUMN_LOC_TYPE", "lType");
define("COLUMN_LOC_SEATING", "lSeatingAvailable");
define("COLUMN_LOC_LAST_UPDATED", "lLastUpdated");

