<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once 'Constants.php';
/*
 * import validateLocation.php as it has the required funtions to check if an item is
 * checked and selected and then echoes out the selected or checked attribute to html
 * it also echoes out the current value or any error messages per field.
 */
require_once 'validateLocation.php';
require_once 'Connection.php';
require_once 'LocationTableGateway.php';

if (!isset($errors)) {
    $errors = array();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!isset($_GET[COLUMN_LOC_ID])) {
        die("Illegal request");
    }

    // to keep it secure filter the id
    $id = filter_input(INPUT_GET, COLUMN_LOC_ID, FILTER_SANITIZE_NUMBER_INT);

    $connection = Connection::getInstance();
    $gateway = new LocationTableGateway($connection);

    $statement = $gateway->getLocationById($id);

    $row = $statement->fetch(PDO::FETCH_ASSOC);

    if (!$row) {
        die("Illegal request");
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (!isset($_POST[COLUMN_LOC_ID])) {
        die("Illegal request");
    }
    if (!isset($formdata)) {
        $formdata = array();
    }
    $row = $formdata;
} else {
    die("Illegal request");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit Location</title>
        <script src="./js/validateLocationForm.js"></script>
        <link rel="stylesheet" href="./css/formStyles.css" type="text/css"></link>
    </head>
    <body>
        <h1>Edit Location</h1>
        <form class="form-style-7" action="editLocation.php" method="post">
            <input type="hidden" name="lID" 
                   value="<?php echo $row[COLUMN_LOC_ID]; ?>" />
            <ul>
                <li>
                    <label for="lName">Location Name</label>
                    <input id="lNameInput" name="lName" type="text" 
                           value="<?php echo $row[COLUMN_LOC_NAME]; ?>"/>
                    <span id="lNameError">
                        <?php echoOutput($errors, COLUMN_LOC_NAME); ?>
                    </span>
                </li>
                <li>
                    <label for="lAddress">Location Address</label>
                    <input id="lAddressInput" name="lAddress" type="text" 
                           value="<?php echo $row[COLUMN_LOC_ADDRESS]; ?>"/>
                    <span id="lAddressError">
                        <?php echoOutput($errors, COLUMN_LOC_ADDRESS); ?>
                    </span>
                </li>
                <li>
                    <label for="lCapacity">Location Capacity</label>
                    <input id="lCapacityInput" name="lCapacity" type="number" 
                           value="<?php echo $row[COLUMN_LOC_CAPACITY]; ?>"/>
                    <span id="lCapacityError">
                        <?php echoOutput($errors, COLUMN_LOC_CAPACITY); ?>
                    </span>
                </li>
                <li>
                    <label for="lmName">Location Manager Name</label>
                    <input id="lmNameInput" name="lmName" type="text" 
                           value="<?php echo $row[COLUMN_LOC_MAN_NAME]; ?>"/>
                    <span id="lmNameError">
                        <?php echoOutput($errors, COLUMN_LOC_MAN_NAME); ?>
                    </span>
                </li>
                <li>
                    <label for="lmEmail">Location Manager Email</label>
                    <input id="lmEmailInput" name="lmEmail" type="email" 
                           value="<?php echo $row[COLUMN_LOC_MAN_EMAIL]; ?>"/>
                    <span id="lmEmailError">
                        <?php echoOutput($errors, COLUMN_LOC_MAN_EMAIL); ?>
                    </span>
                </li>
                <li>
                    <label for="lmPhoneNumber">Location Manager Phone Number</label>
                    <input id="lmPhoneNumberInput" name="lmPhoneNumber" type="text" 
                           value="<?php echo $row[COLUMN_LOC_MAN_PHONE]; ?>"/>
                    <span id="lmPhoneNumberError">
                        <?php echoOutput($errors, COLUMN_LOC_MAN_PHONE); ?>
                    </span>
                </li>

                <li>
                    <label>Seating Available</label>
                    <input type="radio"
                           id="yesSeating"
                           name="lSeatingAvailable"
                           value="1"
                           <?php echoChecked($row, COLUMN_LOC_SEATING, '1'); ?>
                           />
                    <label class="radioLabel" for="Yes">Yes</label>

                    <input type="radio"
                           id="noSeating"
                           name="lSeatingAvailable"
                           value="0"
                           <?php echoChecked($row, COLUMN_LOC_SEATING, '0'); ?>
                           />
                    <label class="radioLabel" for="No">No</label>
                    <span id="lSeatingError">
                        <?php echoOutput($errors, COLUMN_LOC_SEATING); ?>
                    </span>
                </li>
                <li>
                    <label for="lType">Location Type</label>
                    <select id="lTypeInput" name="lType">
                        <option value="invalid"
                        <?php echoSelected($row, COLUMN_LOC_TYPE, 'invalid'); ?>
                                >Select a location type</option>
                        <option value="Indoor"
                        <?php echoSelected($row, COLUMN_LOC_TYPE, 'Indoor'); ?>
                                >Indoor</option>
                        <option value="Outdoor"
                        <?php echoSelected($row, COLUMN_LOC_TYPE, 'Outdoor'); ?>
                                >Outdoor</option>
                        <option value="Both"
                        <?php echoSelected($row, COLUMN_LOC_TYPE, 'Both'); ?>
                                >Both</option>
                    </select>
                    <span id="lTypeError">
                        <?php echoOutput($errors, COLUMN_LOC_TYPE); ?>
                    </span>
                </li>

                <li>
                    <input id="submitBtn" type="submit"/>
                </li>
            </ul>
        </form>
    </body>
</html>
