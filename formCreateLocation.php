<!DOCTYPE html>
<?php
require_once 'Constants.php'; // import constants.php
/*
 * import validateLocation.php as it has the required funtions to check if an item is
 * checked and selected and then echoes out the selected or checked attribute to html
 * it also echoes out the current value or any error messages per field.
 */
require_once 'validateLocation.php';

if (!isset($formdata)) {
    $formdata = array();
}
if (!isset($errors)) {
    $errors = array();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Location</title>
        <script src="./js/validateLocationForm.js"></script>
        <link rel="stylesheet" href="./css/formStyles.css" type="text/css"></link>
        <style>


        </style>
    </head>
    <body>
        <h1>Create Location Form</h1>
        <form class="form-style-7" action="createLocation.php" name="createLocationForm" method="post">

            <ul>
                <li>
                    <label for="lName">Location Name</label>
                    <input id="lNameInput" name="lName" type="text" 
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_NAME); ?>"/>
                    <span id="lNameError">
                        <?php echo echoOutput($errors, COLUMN_LOC_NAME); ?>
                    </span>
                </li>
                <li>
                    <label for="lAddress">Location Address</label>
                    <input id="lAddressInput" name="lAddress" type="text" 
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_ADDRESS); ?>"/>
                    <span id="lAddressError">
                        <?php echo echoOutput($errors, COLUMN_LOC_ADDRESS); ?>
                    </span>
                </li>
                <li>
                    <label for="lCapacity">Location Capacity</label>
                    <input id="lCapacityInput" name="lCapacity" type="number"  
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_CAPACITY); ?>"/>
                    <span id="lCapacityError">
                        <?php echo echoOutput($errors, COLUMN_LOC_CAPACITY); ?>
                    </span>
                </li>
                <li>
                    <label for="lmName">Location Manager Name</label>
                    <input id="lmNameInput" name="lmName" type="text"  
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_MAN_NAME); ?>"/>
                    <span id="lmNameError">
                        <?php echo echoOutput($errors, COLUMN_LOC_MAN_NAME); ?>
                    </span>
                </li>
                <li>
                    <label for="lmEmail">Location Manager Email</label>
                    <input id="lmEmailInput" name="lmEmail" type="email"  
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_MAN_EMAIL); ?>"/>
                    <span id="lmEmailError">
                        <?php echo echoOutput($errors, COLUMN_LOC_MAN_EMAIL); ?>
                    </span>
                </li>
                <li>
                    <label for="lmPhoneNumber">Location Manager Phone Number</label>
                    <input id="lmPhoneNumberInput" name="lmPhoneNumber" type="text"  
                           value="<?php echo echoOutput($formdata, COLUMN_LOC_MAN_PHONE); ?>"/>
                    <span id="lmPhoneNumberError">
                        <?php echo echoOutput($errors, COLUMN_LOC_MAN_PHONE); ?>
                    </span>
                </li>


                <li>
                    <label for="lSeatingAvailable">Seating Available</label>
                    <input type="radio"
                           id="yesSeating"
                           name="lSeatingAvailable"
                           value="1" <?php echoChecked($formdata, COLUMN_LOC_SEATING, '1'); ?>/>
                    <label class="radioLabel" for="1">Yes</label>

                    <input type="radio"
                           id="noSeating"
                           name="lSeatingAvailable"
                           value="0" <?php echoChecked($formdata, COLUMN_LOC_SEATING, '0'); ?>
                           />
                    <label class="radioLabel" for="0">No</label>

                    <span id="lSeatingError">
                        <?php echo echoOutput($errors, COLUMN_LOC_SEATING); ?>
                    </span>
                </li>
                <li>
                    <label for="lType">Location Type</label>

                    <select id="lTypeInput" name="lType">
                        <option value="invalid" <?php echoSelected($formdata, COLUMN_LOC_TYPE, 'invalid'); ?>>Select a location type</option>
                        <option value="Indoor" <?php echoSelected($formdata, COLUMN_LOC_TYPE, 'Indoor'); ?>>Indoor</option>
                        <option value="Outdoor" <?php echoSelected($formdata, COLUMN_LOC_TYPE, 'Outdoor'); ?>>Outdoor</option>
                        <option value="Both" <?php echoSelected($formdata, COLUMN_LOC_TYPE, 'Both'); ?>>Both</option>
                    </select>

                    <span id="lTypeError">
                        <?php echo echoOutput($errors, COLUMN_LOC_TYPE); ?>
                    </span>
                </li>

                <li>
                    <input id="submitBtn" type="submit"/>
                </li>
            </ul>
        </form>
    </body>
</html>
