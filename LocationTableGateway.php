<?php

/**
 * LocationTableGateway is used for connecting and making changes to the database
 * or reading all information from the database.
 *
 * @author Gareth
 */
/* The constants.php file is used to store all row names and table name, 
 * this is so that no mistakes are made when using the column or table names.
 */
require_once 'Constants.php';
require_once 'Location.php';

class LocationTableGateway {

    private $connection;

    // contructor requires a database connection
    public function __construct($connection) {
        $this->connection = $connection;
    }

    /*
     * Gets all the locations rows and column data, it then returns this data to the caller
     */

    public function getLocations() {
        // SQL query string to select all rows and columns from the location table
        $sqlQuery = "SELECT * FROM " . LOCATION_TABLE_NAME;

        $statement = $this->connection->prepare($sqlQuery);
        $status = $statement->execute();

        if (!$status) {
            die("Could not retrieve locations");
        }

        // as long as the data was retrieved from the DB return the statemnt which contains all the data
        return $statement;
    }

    /*
     * Gets the location by the sepcified ID passed into the function
     */

    public function getLocationById($id) {
        // store the SQL query which gets the location by its ID, use placeholders to prevent SQL injection attacks
        $sqlQuery = "SELECT * FROM " . LOCATION_TABLE_NAME . " WHERE " . COLUMN_LOC_ID . " = :id";

        $statement = $this->connection->prepare($sqlQuery);
        // create the placeholder params array
        $params = array(
            "id" => $id
        );

        $status = $statement->execute($params);

        if (!$status) {
            die("Could not retrieve user");
        }

        return $statement;
    }

    /*
     * Inserts a new location row into the database
     */

    public function insertLocation(Location $location) {
        // build sql query use the constants for the column names, use placeholders to protect against SQL injection attacks
        $sqlQuery = "INSERT INTO " . LOCATION_TABLE_NAME .
                " (" . COLUMN_LOC_NAME .
                "," . COLUMN_LOC_ADDRESS
                . "," . COLUMN_LOC_CAPACITY
                . "," . COLUMN_LOC_MAN_NAME
                . "," . COLUMN_LOC_MAN_EMAIL
                . "," . COLUMN_LOC_MAN_PHONE
                . "," . COLUMN_LOC_TYPE
                . "," . COLUMN_LOC_SEATING
                . "," . COLUMN_LOC_LAST_UPDATED . ") "
                . "VALUES (:name, :address, :capacity, :lmName, :lmEmail, :lmPhoneNumber, :lType, :lSeating, :lUpdated)";

        $statement = $this->connection->prepare($sqlQuery);

        // fill the parameters for the placeholders in the query with values from location object
        $params = array(
            "name" => $location->getName(),
            "address" => $location->getAddress(),
            "capacity" => $location->getCapacity(),
            "lmName" => $location->getLocationManagerName(),
            "lmEmail" => $location->getLocationManagerEmail(),
            "lmPhoneNumber" => $location->getLocationManagerPhoneNumber(),
            "lType" => $location->getLocationType(),
            "lSeating" => $location->isSeatingAvailable(),
            "lUpdated" => $location->getLocationLastUpdated()
        );

        $status = $statement->execute($params);

        if (!$status) {
            die("Could not insert location");
        }

        $id = $this->connection->lastInsertId();

        return $id;
    }

    /*
     * Delete a location by the ID passed in
     */

    public function deleteLocation($id) {
        $sqlQuery = "DELETE FROM " . LOCATION_TABLE_NAME . " WHERE " . COLUMN_LOC_ID . " = :id";

        $statement = $this->connection->prepare($sqlQuery);
        $params = array(
            "id" => $id
        );

        $status = $statement->execute($params);

        if (!$status) {
            die("Could not delete location");
        }

        return ($statement->rowCount() == 1); // if successfull it will have deleted one row
    }

    /*
     * Updates an existing location row within the location table.
     */

    public function updateLocation(Location $location) {
        // build sql query use the constants for the column names, use placeholders to protect against SQL injection attacks
        $sqlQuery = "UPDATE " . LOCATION_TABLE_NAME
                . " SET " . COLUMN_LOC_NAME . " = :name, "
                . COLUMN_LOC_ADDRESS . " = :address, "
                . COLUMN_LOC_CAPACITY . " = :capacity, "
                . COLUMN_LOC_MAN_NAME . " = :lmName, "
                . COLUMN_LOC_MAN_EMAIL . " = :lmEmail, "
                . COLUMN_LOC_MAN_PHONE . " = :lmPhoneNumber, "
                . COLUMN_LOC_TYPE . " = :lType, "
                . COLUMN_LOC_SEATING . " = :lSeatingAvailable, "
                . COLUMN_LOC_LAST_UPDATED . " = :lLastUpdated "
                . "WHERE " . COLUMN_LOC_ID . " = :id";

        $statement = $this->connection->prepare($sqlQuery);

        // fill the parameters for the placeholders in the query with values from location object
        $params = array(
            "id" => $location->getID(),
            "name" => $location->getName(),
            "address" => $location->getAddress(),
            "capacity" => $location->getCapacity(),
            "lmName" => $location->getLocationManagerName(),
            "lmEmail" => $location->getLocationManagerEmail(),
            "lmPhoneNumber" => $location->getLocationManagerPhoneNumber(),
            "lType" => $location->getLocationType(),
            "lSeatingAvailable" => $location->isSeatingAvailable(),
            "lLastUpdated" => $location->getLocationLastUpdated(),
        );

        $status = $statement->execute($params);

        return ($statement->rowCount() == 1);
    }

}
