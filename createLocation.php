<?php

/*
 * This file is called on the submit button click of the formCreateLocation.php
 * It calls the validate function of the validateLocation.php file to sanitize and
 * validate the forms data.
 */

//print_r($_POST);

require_once 'Connection.php';
require_once 'Constants.php';
require_once 'LocationTableGateway.php';
require_once 'validateLocation.php';



$formdata = array(); // initialize array to hold sanatized data
$errors = array(); // inittailize errors array to hold errors per input

validate($formdata, $errors);

/*
 * If their are no errors then insert the location into the DB, otherwise go back to the formCreateLocation.php
 */
if (empty($errors)) {

    // get the values for each field using the formdata array
    $name = $formdata[COLUMN_LOC_NAME];
    $address = $formdata[COLUMN_LOC_ADDRESS];
    $capacity = $formdata[COLUMN_LOC_CAPACITY];
    $lmName = $formdata[COLUMN_LOC_MAN_NAME];
    $lmEmail = $formdata[COLUMN_LOC_MAN_EMAIL];
    $lmPhoneNumber = $formdata[COLUMN_LOC_MAN_PHONE];
    $lType = $formdata[COLUMN_LOC_TYPE];
    $lSeating = $formdata[COLUMN_LOC_SEATING];
    $lUpdated = $formdata[COLUMN_LOC_LAST_UPDATED];

    // can set the id to -1 as its not used and the object is only for passing to the insertLocation function
    $location = new Location(-1, $name, $address, $capacity, $lmName, $lmEmail, $lmPhoneNumber, $lType, $lSeating, $lUpdated);

    $connection = Connection::getInstance();
    $gateway = new LocationTableGateway($connection);
    $id = $gateway->insertLocation($location); // insert the location row into the table
    header('Location: index.php'); // redirect the user back to the home page which displays the table
} else {
    // if erros are present return back to the formCreateLocation.php page
    require 'formCreateLocation.php';
}
//


