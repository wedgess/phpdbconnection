<?php

/*
 * This Location file holds all the information about a location as an object,
 * this makes it easier to pass in a location when creating one or updating rather
 * than passing in each field individually.
 * @author Gareth
 */

class Location {

    private $id;
    private $name;
    private $address;
    private $capacity;
    private $locationManagerName;
    private $locationManagerEmail;
    private $locationManagerPhoneNumber;
    private $locationType;
    private $locationSeatingAvailable;
    private $locationLastUpdated;

    public function __construct($id, $name, $address, $capacity, $lmName, $lmEmail, $lmPhoneNumber, $locationType, $locationSeatingAvailable, $locationLastUpdated) {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
        $this->locationManagerName = $lmName;
        $this->locationManagerEmail = $lmEmail;
        $this->locationManagerPhoneNumber = $lmPhoneNumber;
        $this->locationType = $locationType;
        $this->locationSeatingAvailable = $locationSeatingAvailable;
        $this->locationLastUpdated = $locationLastUpdated;
    }

    public function getID() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getCapacity() {
        return $this->capacity;
    }

    public function getLocationManagerName() {
        return $this->locationManagerName;
    }

    public function getLocationManagerEmail() {
        return $this->locationManagerEmail;
    }

    public function getLocationManagerPhoneNumber() {
        return $this->locationManagerPhoneNumber;
    }

    public function getLocationType() {
        return $this->locationType;
    }

    public function isSeatingAvailable() {
        return $this->locationSeatingAvailable;
    }

    public function getLocationLastUpdated() {
        return $this->locationLastUpdated;
    }

}
