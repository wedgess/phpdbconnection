<!DOCTYPE html>
<!--
Home page used to display all the rows and columns with the database as a table.
-->
<?php
require_once 'Location.php';
require_once 'Connection.php';
require_once 'Constants.php';
require_once 'LocationTableGateway.php';


$connection = Connection::getInstance();
$gateway = new LocationTableGateway($connection);

$statement = $gateway->getLocations();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/tableStyles.css"/>
        <script type="text/javascript" src="js/deleteLocationRow.js"></script>
        <title></title>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Capacity</th>
                    <th>Location Manager Name</th>
                    <th>Location Manager Email</th>
                    <th>Location Manager Phone Number</th>
                    <th>Location Type</th>
                    <th>Location Seating</th>
                    <th>Last Updated</th>
                    <!-- action row which contains the create button -->
                    <th><a href="formCreateLocation.php" class="action_button"><img class="add_img" src="images/add_white.png"> Create Location</a></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $row = $statement->fetch(PDO::FETCH_ASSOC);
                while ($row) {
                    // row contains an array of the data in the row, use contants for column names to avoid mistakes
                    echo '<tr>';
                    echo '<td>' . $row[COLUMN_LOC_ID] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_NAME] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_ADDRESS] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_CAPACITY] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_MAN_NAME] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_MAN_EMAIL] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_MAN_PHONE] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_TYPE] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_SEATING] . '</td>';
                    echo '<td>' . $row[COLUMN_LOC_LAST_UPDATED] . '</td>';
                    // cell which contains the edit and delete action
                    echo '<td>';
                    // use the id name as in the table rather than the parameter for get being id
                    echo '<a href="viewLocation.php?' . COLUMN_LOC_ID . '=' . $row[COLUMN_LOC_ID] . '" class="action_button"><img class="img_btn" src="./images/view.png"></a>';
                    echo '<a href="formEditLocation.php?' . COLUMN_LOC_ID . '=' . $row[COLUMN_LOC_ID] . '" class="action_button"><img class="img_btn" src="./images/edit.png"></a>';
                    echo '<a href="deleteLocation.php?' . COLUMN_LOC_ID . '=' . $row[COLUMN_LOC_ID] . '" class="action_button delete_button"><img class="img_btn" src="./images/delete.png"></a>';
                    echo '</td>';
                    echo '</tr>';

                    // get the next rows data, without this we cause an infinte loop of the first row
                    $row = $statement->fetch(PDO::FETCH_ASSOC);
                }
                ?>
            </tbody>
        </table>
    </body>
</html>

